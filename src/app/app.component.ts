import {Component, OnInit} from '@angular/core';
import {UserService} from './shared/service/user.service';
import {ProductService} from './shared/service/product.service';
import {CartService} from './shared/service/cart.service';
import {Cart} from './shared/models/cart';
import {Roles} from './shared/roles';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  basket = 0;
  isLoggedIn = false;
  isAdmin = false;
  constructor(private userService: UserService, private productService: ProductService, private cartService: CartService) {
  }

  ngOnInit(): void {
    const token = this.userService.getToken();
    const role = this.userService.getUserRole();
    this.userService.loggedIn.subscribe(bol => {
      this.isLoggedIn = bol;
    });
    this.userService.isAdmin.subscribe(bol => {
      this.isAdmin = bol;
    });
    if (token) {
      this.getBasket();
      this.isLoggedIn = true;
    }
    if (role === Roles.Admin) {
      this.isAdmin = true;
    }
    this.cartService.refetchCarts.subscribe((value: boolean) => {
      console.log(value)
      if (value) {
        this.getBasket();
      }
    });
  }
  getBasket() {
    this.cartService.getBasketTotal().subscribe((qte: number) => {
      this.basket = qte;
    });
  }

  logout(): void {
    this.userService.logout();
  }
}
