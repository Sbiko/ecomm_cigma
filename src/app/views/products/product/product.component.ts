import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../../shared/service/product.service';
import {Product} from '../../../shared/models/product';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  productStatus = false;
  products: Product[];
  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit() {
   this.getProducts();
  }
  getProducts() {
    this.productService.getProducts().subscribe((products: Product[]) => {
      this.products = products;
    });
  }
  editProduct(id) {
    this.router.navigate(['/admin/product/edit/', id]);
  }

  toAddProduct() {
    this.router.navigate(['/admin/product/create' ]);
  }
  deleteProduct(id) {
    this.productService.deleteProduct(id).subscribe(result => {
      this.getProducts();
    }, error => {
      if (error.status === 500) {
        this.productStatus = true;
        setTimeout(() => {
          this.productStatus = false;
        }, 3000);
      }
    });
  }
}
