import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Product} from '../../../../shared/models/product';
import {ProductService} from '../../../../shared/service/product.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  searchMode = false;
  @Input() products: Product[];
  constructor(private productService: ProductService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const token = JSON.parse(localStorage.getItem('token'));
    if (token) {
     this.handleProducts();
    }
  }
  selectProductAndNavigate(id) {
    this.router.navigate(['/cart/' + id]);
  }

  handleProducts() {
    this.searchMode = this.route.snapshot.paramMap.has('keyword');
    this.searchMode ? this.getProductsByKeyword() : this.getProducts();

  }

  getProducts() {
    this.productService.getProducts().subscribe((products: Product[]) => {
      this.products = products;
    });
  }

  getProductsByKeyword() {
    this.route.params.subscribe(param => {
      this.productService.getProductsByKeyword(param.keyword).subscribe((products: Product[]) => {
        this.products = products;
      });
    });
  }
}
