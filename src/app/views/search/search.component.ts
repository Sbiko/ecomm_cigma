import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Input() isLoggedIn = false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  doSearch(value) {
    console.log(value)
    this.router.navigateByUrl(`/search/${value}`);
  }
}
